package com.github.axet.smsgate.app;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

public class ScheduleSMS extends ScheduleTime {
    public String phone;
    public String message;
    public int sim; // sim id; -1 == default
    public boolean hide;

    public ScheduleSMS(ScheduleSMS s) {
        super(s);
        this.phone = s.phone;
        this.message = s.message;
        this.hide = s.hide;
    }

    public ScheduleSMS(Context context) {
        super(context);
        sim = -1;
    }

    public ScheduleSMS(Context context, String json) {
        super(context, json);
        try {
            JSONObject o = new JSONObject(json);
            phone = o.getString("phone");
            message = o.getString("message");
            sim = o.getInt("sim");
            hide = o.optBoolean("hide", false);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public JSONObject save() {
        try {
            JSONObject o = super.save();
            o.put("phone", phone);
            o.put("message", message);
            o.put("sim", sim);
            o.put("hide", hide);
            return o;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }
}
