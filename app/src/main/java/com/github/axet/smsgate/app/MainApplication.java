package com.github.axet.smsgate.app;

import android.app.Notification;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.internet.MimeMessage;
import com.fsck.k9.mail.internet.TextBody;
import com.github.axet.androidlibrary.crypto.Bitcoin;
import com.github.axet.androidlibrary.services.FileProvider;
import com.github.axet.smsgate.providers.SIM;
import com.github.axet.smsgate.services.FirebaseService;
import com.github.axet.smsgate.services.NotificationService;
import com.github.axet.smsgate.widgets.ApplicationsPreference;
import com.google.firebase.database.FirebaseDatabase;
import com.zegoggles.smssync.App;
import com.zegoggles.smssync.mail.Headers;
import com.zegoggles.smssync.preferences.AuthPreferences;
import com.zegoggles.smssync.service.SmsBackupService;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.fsck.k9.mail.internet.MimeMessageHelper.setBody;

public class MainApplication extends App {
    public static final String SCHEDULER_COUNT = "SCHEDULER_COUNT";
    public static final String SCHEDULER_ITEM = "SCHEDULER_";
    public static final String PUB = "PUB";
    public static final String SEC = "SEC";
    public static final String SMS_LAST = "SMS_LAST";
    public static final String MMS_LAST = "MMS_LAST";
    public static final String PREF_FIREBASE = "firebase";
    public static final String PREF_NOTIFICATION_LISTENER = "notification_listener";
    public static final String PREF_WIFIRESTART = "wifi_restart";
    public static final String PREF_WIFI = "wifi_only";
    public static final String PREF_ADMIN = "admin";
    public static final String PREF_OPTIMIZATION = "optimization";
    public static final String PREF_OPTIMIZATION_WARNING = "optimization_warning";
    public static final String PREF_NEXT = "next";
    public static final String PREF_DEFAULTSMS = "defaultsms";
    public static final String PREF_FIREBASESETTINGS = "firebase_settings";
    public static final String PREF_REBOOT = "reboot";

    public static final String PREF_APPS = "applications"; // enabled
    public static final String APPS_INDEX = "APPS_";
    public static final String APPS_COUNT = "APPS_COUNT";

    public static final String APP_FROM = "from";
    public static final String APP_SUBJ = "subject";
    public static final String APP_BODY = "body";

    public static class NotificationInfo extends NotificationService.NotificationInfo {
        String pkg;
        String nid;
        Notification n;

        public NotificationInfo(String id) {
            super(id);
        }
    }

    SIM sim;
    Bitcoin keyPair = null;
    Handler handler = new Handler();
    NotificationService.NotificationsMap<NotificationInfo> lastId = new NotificationService.NotificationsMap<>(handler);

    public static boolean firebaseEnabled(Context context) {
        if (Build.VERSION.SDK_INT < 11) // WebInterface requires P2P encryption API11+ EC KeyFactory
            return false;
        ComponentName name = new ComponentName(context, "com.google.firebase.provider.FirebaseInitProvider");
        PackageManager pm = context.getPackageManager();
        if (pm.getComponentEnabledSetting(name) == PackageManager.COMPONENT_ENABLED_STATE_DISABLED)
            return false;
        try {
            if (!pm.getProviderInfo(name, PackageManager.MATCH_DEFAULT_ONLY).enabled)
                return false;
        } catch (PackageManager.NameNotFoundException ignore) {
            return false;
        }
        try {
            Class.forName("com.google.firebase.FirebaseFake");
            return false;
        } catch (ClassNotFoundException e) {
            return true;
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

        boolean detectedFirebase_10_2 = false;
        try {
            FirebaseDatabase.class.getMethod("getInstance", String.class); // new method added FirebaseDatabase.getInstance(String url)
            detectedFirebase_10_2 = true;
        } catch (NoSuchMethodException e) {
        }
        if (Build.VERSION.SDK_INT < 14 && detectedFirebase_10_2) { // disable firebase for API14< and firebase10.2+
            ComponentName name = new ComponentName(base, "com.google.firebase.provider.FirebaseInitProvider");
            PackageManager pm = getPackageManager();
            pm.setComponentEnabledSetting(name, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, 0);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (Build.VERSION.SDK_INT >= 11) {
            keyPair = new Bitcoin(); // min API11+ EC KeyFactory
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            String sec = prefs.getString(SEC, "");
            String pub = prefs.getString(PUB, "");

            if (sec.isEmpty() || pub.isEmpty()) {
                keyPair.generate();
                save();
            } else {
                keyPair.loadSec(sec);
                keyPair.loadPub(pub);
            }
        }
    }

    public void save() {
        if (keyPair != null) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor edit = prefs.edit();
            edit.putString(SEC, keyPair.saveSec());
            edit.putString(PUB, keyPair.savePub());
            edit.commit();
        }
    }

    public SIM getSIM() {
        if (sim == null) {
            sim = new SIM(this);
        }
        return sim;
    }

    public Bitcoin getKeyPair() {
        return keyPair;
    }

    public static MainApplication getApp(Context context) {
        return (MainApplication) context.getApplicationContext();
    }

    public static List<ScheduleSMS> load(Context context) {
        List<ScheduleSMS> items = new ArrayList<>();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        int count = prefs.getInt(SCHEDULER_COUNT, 0);
        for (int i = 0; i < count; i++) {
            String state = prefs.getString(SCHEDULER_ITEM + i, "");
            ScheduleSMS item = new ScheduleSMS(context, state);
            items.add(item);
        }

        return items;
    }

    public static void save(Context context, List<ScheduleSMS> items) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putInt(SCHEDULER_COUNT, items.size());
        for (int i = 0; i < items.size(); i++) {
            ScheduleSMS item = items.get(i);
            edit.putString(SCHEDULER_ITEM + i, item.save().toString());
        }
        edit.commit();
    }

    public static Uri shareFile(Context context, String type, String name, byte[] buf) {
        try {
            File outputDir = context.getCacheDir();
            File outputFile = File.createTempFile("share", ".tmp", outputDir);
            IOUtils.write(buf, new FileOutputStream(outputFile));
            return FileProvider.getUriForFile(context, type, name, outputFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void shareClear(Context context) {
        File outputDir = context.getCacheDir();
        File[] ff = outputDir.listFiles();
        if (ff != null) {
            for (File f : ff) {
                if (f.getName().startsWith("share"))
                    f.delete();
            }
        }
    }

    public static String toHexString(int l) {
        return String.format("%08X", l);
    }

    public static String toHexString(long l) {
        return String.format("%016X", l);
    }

    public static void AppsNotification(Context context, String action, String pkg, String id, Notification n) {
        if (Build.VERSION.SDK_INT >= 16) {
            if (n.priority <= Notification.PRIORITY_LOW)
                return; // ignore low priority notifications
        }
        MainApplication app = (MainApplication) context.getApplicationContext();
        if (action.equals(NotificationService.REMOVE)) {
            app.lastId.remove(pkg);
            return;
        }
        AuthPreferences auth = new AuthPreferences(context);
        boolean connected = auth.hasOauthTokens() || auth.hasOAuth2Tokens();
        if (!connected)
            return;
        AppsAdd(context, pkg, n);
    }

    public static void AppsAdd(final Context context, String pkg, Notification n) {
        String applicationName = FirebaseService.getApplicationName(context, pkg);

        String title = null;
        String text;
        String details = null;

        if (Build.VERSION.SDK_INT >= 19) {
            title = FirebaseService.toString(n.extras.get("android.title"));
            text = FirebaseService.toString(n.extras.get("android.text")); // can be spannable
            if (text == null || text.isEmpty()) {
                text = FirebaseService.toString(n.tickerText); // can be null
            }
            details = FirebaseService.toString(n.extras.get("android.bigText")); // class android.text.SpannableString
        } else {
            text = FirebaseService.toString(n.tickerText); // can be null
        }

        if (title == null || title.isEmpty()) {
            title = text;
            if (title == null || title.isEmpty())
                title = details;
            if (title == null || title.isEmpty())
                title = "";
            if (details == null || details.isEmpty())
                details = text;
            if (details == null || details.isEmpty())
                details = "";
        } else if (details == null || details.isEmpty()) {
            details = text;
            if (details == null || details.isEmpty())
                details = "";
        }

        MainApplication app = (MainApplication) context.getApplicationContext();

        long now = System.currentTimeMillis();

        if (Build.VERSION.SDK_INT >= 18) { // old api has only UPDATE events and no REMOVE, add them all
            int code = (title + " " + details).hashCode();
            NotificationInfo last = app.lastId.get(pkg);
            if (last == null)
                last = new NotificationInfo(pkg);

            if (app.lastId.duplicate(last, code))
                return; // do not email same text twice

            last.pkg = pkg;
            last.n = n;

            if (app.lastId.delayed(last, now)) {
                final NotificationInfo info = last;
                app.lastId.delay(last, new Runnable() {
                    @Override
                    public void run() {
                        AppsAdd(context, info.pkg, info.n);
                    }
                });
                return;
            }

            app.lastId.put(last, code, now);
        }

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Set<String> ss = ApplicationsPreference.load(prefs.getString(MainApplication.PREF_APPS, ""));
        if (!ApplicationsPreference.contains(ss, pkg))
            return;
        int i = prefs.getInt(APPS_COUNT, 0);

        for (int j = 0; j < i; j++) {
            try {
                String json = prefs.getString(APPS_INDEX + j, "");
                JSONObject o = new JSONObject(json);
                String from = o.getString(APP_FROM);
                String subj = o.getString(APP_SUBJ);
                String body = o.getString(APP_BODY);
                if (from.equals(applicationName) && subj.equals(title) && body.equals(details))
                    return;// duplicate
            } catch (JSONException e) {
                Log.d(TAG, "unable to process message", e); // ignore
            }
        }

        int count = i + 1;

        try {
            JSONObject json = new JSONObject();
            json.put(APP_FROM, applicationName);
            json.put(APP_SUBJ, title);
            json.put(APP_BODY, details);
            SharedPreferences.Editor edit = prefs.edit();
            edit.putString(APPS_INDEX + i, json.toString());
            edit.putInt(APPS_COUNT, count);
            edit.commit();
        } catch (JSONException e) {
            Log.d(TAG, "unable to process message", e); // ignore
        }

        SmsBackupService.scheduleBackup(context, 3); // 3 sec delay
    }

    public static List<Message> AppsMessages(Context context, String userEmail) {
        ArrayList<Message> list = new ArrayList<>();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        int count = prefs.getInt(APPS_COUNT, 0);
        for (int i = 0; i < count; i++) {
            try {
                final Message msg = new MimeMessage();
                String json = prefs.getString(APPS_INDEX + i, "");
                JSONObject o = new JSONObject(json);
                String from = o.getString(APP_FROM);
                String subj = o.getString(APP_SUBJ);
                msg.setFrom(new Address(userEmail, from));
                msg.setRecipient(Message.RecipientType.TO, new Address(userEmail));
                msg.setSubject(subj);
                msg.setHeader(Headers.REFERENCES, String.format("<%d.%d@sms-backup-plus.local>", from.hashCode(), subj.hashCode()));
                setBody(msg, new TextBody(o.getString(APP_BODY)));
                list.add(msg);
            } catch (Exception e) {
                Log.d(TAG, "unable to process message", e); // ignore
            }
        }

        return list;
    }

    public static int AppsCount(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt(APPS_COUNT, 0);
    }

    public static int AppsDeleteAll(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        int count = prefs.getInt(APPS_COUNT, 0);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putInt(APPS_COUNT, 0);
        edit.commit();
        return count;
    }
}
