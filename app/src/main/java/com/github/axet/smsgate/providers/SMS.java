package com.github.axet.smsgate.providers;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.telephony.SmsManager;

import com.github.axet.smsgate.mediatek.SmsManagerMT;
import com.zegoggles.smssync.SmsConsts;

import java.util.ArrayList;
import java.util.Date;

/**
 * https://github.com/shutoff/ugona.net/blob/master/src/main/java/net/ugona/plus/Sms.java
 */
public class SMS {
    public static final Uri SMS_PROVIDER = Uri.parse("content://sms/sent");

    public static final int MESSAGE_TYPE_SENT = 2; // Telephony.TextBasedSmsColumns.MESSAGE_TYPE_SENT

    public static String filterPhone(String phone) {
        phone = phone.replaceAll(" ", "");
        phone = phone.replaceAll("-", "");
        phone = phone.replaceAll("\\(", "");
        phone = phone.replaceAll("\\)", "");
        return phone;
    }

    public static void store(Context context, String phone, String msg, Date date, String thread) {
        if (date == null)
            date = new Date();
        ContentResolver res = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(SmsConsts.BODY, msg);
        values.put(SmsConsts.ADDRESS, phone);
        values.put(SmsConsts.TYPE, MESSAGE_TYPE_SENT);
        values.put(SmsConsts.PROTOCOL, 0);
        values.put(SmsConsts.SERVICE_CENTER, "");
        values.put(SmsConsts.DATE, date.getTime());
        values.put(SmsConsts.STATUS, -1);
        if (thread != null)
            values.put(SmsConsts.THREAD_ID, thread);
        values.put(SmsConsts.READ, 1);
        res.insert(SMS_PROVIDER, values);
    }

    public static void send(int simID, String phone, String msg) {
        phone = filterPhone(phone);
        if (Build.VERSION.SDK_INT >= 22) {
            SmsManager sm = SmsManager.getSmsManagerForSubscriptionId(simID);
            ArrayList<String> parts = sm.divideMessage(msg);
            if (parts.size() > 1) {
                sm.sendMultipartTextMessage(phone, null, parts, null, null);
            } else {
                sm.sendTextMessage(phone, null, msg, null, null);
            }
            return;
        }

        try {
            SmsManagerMT mt = new SmsManagerMT();
            ArrayList<String> parts = mt.divideMessage(msg);
            if (parts.size() > 1) {
                mt.sendMultipartTextMessage(phone, null, parts, null, null, simID);
            } else {
                mt.sendTextMessage(phone, null, msg, null, null, simID);
            }
            return; // metdatek phone
        } catch (Throwable ignore) {
        }
    }

    public static void send(Context context, int simID, String phone, String msg) {
        send(simID, phone, msg);
        store(context, phone, msg, null, null);
    }

    public static void send(String phone, String msg) {
        phone = filterPhone(phone);
        SmsManager sm = SmsManager.getDefault();
        ArrayList<String> parts = sm.divideMessage(msg);
        if (parts.size() > 1) {
            sm.sendMultipartTextMessage(phone, null, parts, null, null);
        } else {
            sm.sendTextMessage(phone, null, msg, null, null);
        }
    }

    public static void send(Context context, String phone, String msg) {
        send(phone, msg);
        store(context, phone, msg, null, null);
    }
}
